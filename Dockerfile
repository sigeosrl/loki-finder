# Use the official Gradle image as a build environment
FROM gradle:8.5-jdk21 AS build

USER root

# Set the working directory in the Docker container
WORKDIR /app

# Copy the Gradle configuration and source code into the container
COPY --chown=gradle:gradle . .

# Build the Quarkus application
RUN gradle build -x test

RUN gradle sonar \
-Dsonar.host.url=http://sonar.cdpks01.sian.it \
-Dsonar.login=ee3ec636ed8385d3932043cfe331cd7a0da24189 \
-Dsonar.projectKey=poncfa-loki-finder-ms \
-Dsonar.projectName=poncfa-loki-finder-ms

# Use the OpenJDK image as a runtime environment
FROM openjdk:21

# Set the working directory in the Docker runtime container
WORKDIR /app

# Copy the runner jar and the quarkus-app directory
COPY --from=build /app/build/quarkus-app/ /app/quarkus-app/

# Command to run on boot
CMD ["java", "-jar", "/app/quarkus-app/quarkus-run.jar"]
