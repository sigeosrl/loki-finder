package client;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Response;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

@Path("/loki/api/v1")
@RegisterRestClient(configKey="loki-api")
public interface LokiService {

    @GET
    @Path("/query_range")
    Response queryRange(@QueryParam("query") String query,
                        @QueryParam("start") long start,
                        @QueryParam("end") long end,
                        @QueryParam("limit") int limit);
}
