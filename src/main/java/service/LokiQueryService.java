package service;

import client.LokiService;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.Response;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@ApplicationScoped
public class LokiQueryService {

    @Inject
    @RestClient
    LokiService lokiService;

    public Map<String, String> queryLogs(String serviceName, String start, String end, int limit) {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");

        // Use LocalDateTime to parse and manipulate date/time information
        LocalDateTime startDateTime = start != null ? LocalDateTime.parse(start, formatter) : null;
        LocalDateTime endDateTime = end != null ? LocalDateTime.parse(end, formatter) : null;

        // Convert LocalDateTime to epoch seconds for Loki query
        long startInt = startDateTime != null
                ? startDateTime.toEpochSecond(ZoneOffset.UTC)
                : 0; // Default to min epoch time
        long endInt = endDateTime != null
                ? endDateTime.toEpochSecond(ZoneOffset.UTC)
                : Long.MAX_VALUE; // Default to max epoch time

        if (startInt > endInt) {
            log.error("Start time cannot be greater than end time");
            return new HashMap<>();
        }

        String query = "{host=\"" + serviceName + "\"} |= `` | json | line_format `{{.full_message}}`";

        // Enhanced logging for troubleshooting
        log.debug("Constructed Loki query: {}", query);
        log.debug("Start epoch: {}", startInt);
        log.debug("End epoch: {}", endInt);

        Response response = lokiService.queryRange(query, startInt, endInt, limit);

        if (response.getStatus() != 200) {
            log.error("Error querying Loki: {}", response.getStatusInfo().getReasonPhrase());
            return new HashMap<>();
        }

        String responseBody = response.readEntity(String.class);

        log.debug("Response: {}", responseBody);

        if (responseBody == null || responseBody.isEmpty()) {
            log.error("No logs found for the given query");
            return new HashMap<>();
        }

        return extractFullMessageAndDate(responseBody);
    }

    public Map<String, String> extractFullMessageAndDate(String responseBody) {
        Map<String, String> messages = new HashMap<>();
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            JsonNode rootNode = objectMapper.readTree(responseBody);

            JsonNode results = rootNode.path("data").path("result");

            for (JsonNode result : results) {
                JsonNode stream = result.path("stream");
                String fullMessage = stream.has("full_message") ? stream.get("full_message").asText() : "noMessage";
                String date = stream.has("_Time") ? stream.get("_Time").asText() : "noDate";

                if (date.equals("noDate")) {

                    String ts = stream.has("timestamp") ? stream.get("timestamp").asText() : "noDate";

                    if (!ts.equals("noDate")) {
                        try {
                            // Parse the timestamp as a double and convert to milliseconds
                            long milliseconds = (long) (Double.parseDouble(ts) * 1000);
                            Instant instant = Instant.ofEpochMilli(milliseconds);
                            LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
                            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
                            date = localDateTime.format(formatter);
                        } catch (NumberFormatException e) {
                            log.error("Invalid timestamp format: {}", ts);
                            // Handle the error or set a default value for 'date'
                        }
                    }
                }

                // Ensure non-null values for both key and value
                if (fullMessage != null) {
                    log.debug("Full message: {}", fullMessage);
                    messages.put(date, fullMessage);
                }
            }
        } catch (Exception e) {
            log.error("Error processing response: {}", e.getMessage());
        }
        return messages;
    }
}
