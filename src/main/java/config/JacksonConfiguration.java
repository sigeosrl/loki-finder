package config;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.module.SimpleModule;
import io.quarkus.jackson.ObjectMapperCustomizer;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.Produces;

import java.util.Map;

@ApplicationScoped
public class JacksonConfiguration {

    @Produces
    public ObjectMapperCustomizer objectMapperCustomizer() {
        return new ObjectMapperCustomizer() {
            @Override
            public void customize(ObjectMapper objectMapper) {
                // Enable pretty printing
                objectMapper.enable(SerializationFeature.INDENT_OUTPUT);

                // Ignore unknown properties during deserialization to prevent errors
                objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

                // Create a module to hold custom serializers and deserializers
                SimpleModule module = new SimpleModule();

                // Add custom null key serializer
                module.addKeySerializer(Object.class, new NullKeySerializer());

                // Add custom map deserializer
                module.addDeserializer(Map.class, new CustomMapDeserializer());

                // Register the fully configured module
                objectMapper.registerModule(module);
            }
        };
    }
}