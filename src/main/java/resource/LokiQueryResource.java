package resource;

import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import service.LokiQueryService;

@Path("/loki-query")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class LokiQueryResource {

    @Inject
    LokiQueryService lokiQueryService;

    @GET
    public Response getLogs(@QueryParam("serviceName") String serviceName,
                            @QueryParam("start") String start,
                            @QueryParam("end") String end,
                            @QueryParam("limit") int limit) {
        return Response.ok(lokiQueryService.queryLogs(serviceName, start, end, limit)).build();
    }
}
