# Loki finder

## RUN
```bash
docker compose up -d
```

Launch the application with `quarkus -> quarkusDev`...

## Test

GET on `http://localhost:8080/loki-query`.

![query](./images/img.png)


---

Enjoy!